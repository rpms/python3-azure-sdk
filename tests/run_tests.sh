#!/bin/sh

err=0

for lib in azure.mgmt.compute; do
	output=$(python3 -c "import sys; sys.path.append('/usr/share/fence'); \
		sys.path.insert(0, '/usr/lib/fence-agents/bundled/azure'); \
		import $lib" 2>&1)
	if [ $? -ne 0 ]; then
		echo -e "ERROR: Failed to import $lib:\n$output"
		err=$((err+1))
	else
		echo "INFO: importing $lib works..."
	fi
done

if [ $err -ge 1 ]; then
	echo -e "\nERROR: $err tests FAILED..."
	exit 1
fi
